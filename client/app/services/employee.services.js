
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("DMS")
        .service("EmpService", EmpService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    EmpService.$inject = ['$http'];

    // DeptService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function EmpService($http) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
       service.retrieveNonManagers = retrieveNonManagers;


        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // retrieveNonManagers retrieves employees that are currently not department managers from the server via HTTP
        // GET. Parameters: None. Returns: Promise object
        function retrieveNonManagers() {
            return $http({
                method: 'GET'
                , url: 'api/employees'
            });
        }
    }
})();