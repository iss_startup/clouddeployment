// Defines client-side routing
(function () {
    angular
        .module("DMS")
        .config(deptRouteConfig)
    deptRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function deptRouteConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('edit', {
                url: '/edit',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })
            .state('editWithParam', {
                url: '/edit/:deptNo',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: './app/registration/register.html',
                controller: 'RegCtrl',
                controllerAs: 'ctrl'
            })
            .state('search', {
                url: '/search',
                templateUrl: './app/search/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'
            })
            .state('searchDB', {
                url: '/searchDB',
                templateUrl: './app/search/searchDB.html',
                controller: 'SearchDBCtrl',
                controllerAs: 'ctrl'
            })
            .state('thanks', {
                url: '/thanks',
                templateUrl: './app/registration/thanks.html'
            })


        $urlRouterProvider.otherwise("/register");
    }
})();